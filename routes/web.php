<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });


Route::get('/','user\BulletinController@index')->name('index');
Route::post('post','user\BulletinController@store')->name('post');

Route::post('update/{id}','user\BulletinController@editBulletinModal')->name('update.bulletin');
