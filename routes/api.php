<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::get('getid/bulletin/{id}','user\BulletinController@getIdBulletin')->name('get-id-bulletin');
Route::get('getall/bulletin','user\BulletinController@showAllData')->name('get-all-bulletin');
Route::put('edit/bulletin/{id}','user\BulletinController@editBulletin')->name('edit-bulletin');