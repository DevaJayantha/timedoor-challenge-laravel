<footer>
    <p class="font12">
        Copyright &copy; 
        {{ now()->year }}
        by 
        <a href="https://timedoor.net" class="text-green">
            PT. TIMEDOOR INDONESIA
        </a> 
    </p>
</footer>