@extends('user.master')

@section('style-custom')
    <link rel="stylesheet" type="text/css" href="{{ asset('user/css/font-awesome.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('user/css/custom-index.css')}}">  
@endsection

@section('body')
    <div class="section">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-3 bg-white p-30 box">
                    <div>
                        <h1 class="text-green mb-30"><b>Level 8 Challenge</b></h1>
                        @if ($message = Session::get('success'))
                            <div class="alert alert-success alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button> 
                                <strong>{{ $message }}</strong>
                            </div>
                        @endif
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    </div>
                    <form action="{{ route('post')}}" method="post" class="form" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <label>Title</label>
                            <input type="text" name="title" class="form-control" id="title">
                        </div>
                        <div class="form-group">
                            <label>Body</label>
                            <textarea rows="5" name="body" class="form-control" id="body"></textarea>
                        </div>
                        <div class="form-group">
                            <label>Choose image from your computer :</label>
                            <div class="input-group">
                              <input type="text" class="form-control upload-form" value="No file chosen" readonly>
                              <span class="input-group-btn">
                                <span class="btn btn-default btn-file">
                                  <i class="fa fa-folder-open"></i>&nbsp;Browse <input type="file" name="image" accept=".png, .jpg, .jpeg">
                                </span>
                              </span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Password</label>
                            {{-- <input type="password" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" /> --}}
                            <input type="password" class="form-control" name="password">
                        </div>
                        <div class="text-center mt-30 mb-30">
                            <button class="btn btn-primary" type="submit">Submit</button>
                        </div>
                    </form>
                    <hr>
                    @foreach ($userItem as $item)
                        <div class="post">
                            <div class="clearfix">
                                <div class="pull-left">
                                    <h2 class="mb-5 text-green"><b>{{ $item->title }}</b></h2>
                                </div>
                                <div class="pull-right text-right">
                                    <p class="text-lgray">{{ $item->created_at->format('d-m-Y') }}<br/><span class="small">{{ $item->created_at->format('H:i') }}</span></p>
                                </div>
                            </div>
                            <p>{!! nl2br($item->body) !!}</p>
                            <div class="img-box my-10">
                                <img class="img-responsive img-post" src="{{ asset('user/images/'.$item->images)}}" alt="image" style="width: 50%!important">
                            </div>
                            <form class="form-inline mt-50">
                                <div class="form-group mx-sm-3 mb-2">
                                    <label class="sr-only">Password</label>
                                    <input type="password" class="form-control" id="inputPassword{{$item->id}}" placeholder="Password">
                                </div>
                                <a type="submit" class="btn btn-default mb-2 btn-edit-modal" data-toggle="modal" onclick="onClickEdit({{ $item->id }})" id="btnEdit{{ $item->id }}" data-target="#editModal{{ $item ->id}}"><i class="fa fa-pencil p-3"></i></a>
                                <a type="submit" class="btn btn-danger mb-2" data-toggle="modal" data-target="#deleteModal{{ $item->id }}" onclick="onClickDelete({{ $item->id }})"><i class="fa fa-trash p-3"></i></a>
                            </form>
                        </div>
                        <div class="modal fade" id="editModal{{ $item ->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                        <h4 class="modal-title" id="myModalLabel">Edit Item</h4>
                                    </div>
                                    <form action="{{route('update.bulletin',$item->id)}}" enctype="multipart/form-data" id="formDataModal{{ $item->id}}" method="post">
                                        @csrf
                                        <div class="modal-body">
                                            <input type="hidden" name="id" value="{{$item->id}}">
                                            <div class="form-group">
                                                <label>Title</label>
                                                <input id="titleModal{{$item->id}}" type="text" class="form-control" name="title" value="{{ $item->title }}">
                                                {{--  <p class="small text-danger mt-5">*Your title must be 3 to 16 characters long</p>  --}}
                                            </div>
                                            <div class="form-group">
                                                <label>Body</label>
                                                <textarea id="bodyModal{{$item->id}}" name="body" rows="5" class="form-control">{{ $item->body }}</textarea>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-md-4">
                                                    <img class="img-responsive image-modal" alt="" src="{{ asset('user/images/'.$item->images)}}" >
                                                </div>
                                                <div class="col-md-8 pl-0">
                                                    <label>Choose image from your computer :</label>
                                                    <div class="input-group">
                                                        <input type="text" class="form-control upload-form" value="No file chosen" readonly>
                                                        <span class="input-group-btn">
                                                            <span class="btn btn-default btn-file">
                                                            <i class="fa fa-folder-open"></i>&nbsp;Browse <input type="file" id="buttonModal{{$item->id}}" name="image">
                                                            </span>
                                                        </span>
                                                    </div>
                                                    <div class="checkbox">
                                                        <label>
                                                            <input id="checkModal{{$item->id}}" type="checkbox" name="checkbox">Delete image
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group" id="form-group-pass{{$item->id}}">
                                                <label id="textPasswordModal{{$item->id}}">Password</label>
                                                <input type="password" class="form-control" id="inputPasswordModal{{$item->id}}">
                                                <p class="small text-danger mt-5" id="erroMsgPass{{$item->id}}" >*Your password must be 4 digit number</p>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-primary" id="btnSaveModal{{$item->id}}">Save changes</button>
                                            <button type="button" class="btn btn-info" id="btnEditModal{{$item->id}}" onclick="onClickEditModal({{ $item->id }})">Edit</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="modal fade" id="deleteModal{{ $item->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                            <div class="modal-dialog">
                              <div class="modal-content">
                                <div class="modal-header">
                                  <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                  <h4 class="modal-title" id="myModalLabel">Delete Data</h4>
                                </div>
                                <div class="modal-body pad-20">
                                  <p>Are you sure want to delete this item?</p>
                                </div>
                                <div class="modal-footer">
                                  <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                  <button type="button" class="btn btn-danger">Delete</button>
                                </div>
                              </div>
                            </div>
                          </div>
                    @endforeach
                    <div class="text-center mt-30">
                        {{ $userItem->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
/*
        $(document).ready(function(){
            $('#btnEdit'+item_id).click(function(){
                console.log("start");
                var passValue = document.getElementById("inputPassword"+item_id).value;
                console.log(passValue);
            })
        })*/

        function onClickDelete(item_id)
        {
            console.log("check pass delete");
            const passValue = document.getElementById('inputPassword'+item_id).value;
            $.ajax({
                url: 'http://127.0.0.1:8000/api/getid/bulletin/'+item_id,
                type: 'get',
                dataType: 'json',
                success:function(response){
                    const passTrue = response.data;
                    if(passTrue==passValue)
                    {
                        console.log("benar");
                    }else{  
                        console.log("salah");
                    }
                }
            })
        }

        function onClickEdit(item_id)
        {

            console.log("Start verifikasi password 1");
            const passValue = document.getElementById('inputPassword'+item_id).value;
            $('#inputPassword'+item_id).val("");
            $('#inputPasswordModal'+item_id).val("");
            $('#erroMsgPass'+item_id).hide();
            $.ajax({
                url: 'http://127.0.0.1:8000/api/getid/bulletin/'+item_id,
                type: 'get',
                dataType: 'json',
                success: function(response){
                    const passTrue = response.data;
                    if(passValue == passTrue) {
                        $('#form-group-pass'+item_id).hide();
                        $('#btnEditModal'+item_id).hide();
                        $('#btnSaveModal'+item_id).show();
                        $('#titleModal'+item_id).prop('disabled', false);
                        $('#bodyModal'+item_id).prop('disabled', false);
                        $('#buttonModal'+item_id).prop('disabled', false);
                        $('#checkModal'+item_id).prop('disabled', false);

                    } else {
                        $('#form-group-pass'+item_id).show();
                        $('#btnEditModal'+item_id).show();
                        $('#btnSaveModal'+item_id).hide();
                        $('#titleModal'+item_id).prop('disabled', true);
                        $('#bodyModal'+item_id).prop('disabled', true);
                        $('#buttonModal'+item_id).prop('disabled', true);
                        $('#checkModal'+item_id).prop('disabled', true);
                        
                    }
                }
            },8000)
            

        }

        function onClickEditModal(item_id)
        {
            //console.log("Start verifikasi password 2");
            const valuePasswordModal = document.getElementById('inputPasswordModal'+item_id).value;
            $('#inputPasswordModal'+item_id).val("");
            $.ajax({
                url: 'http://127.0.0.1:8000/api/getid/bulletin/'+item_id,
                type: 'get',
                dataType: 'json',
                success: function(response){
                    const passTrue = response.data;
                    if($.isNumeric(valuePasswordModal) == true){
                        
                        if(valuePasswordModal == passTrue) {

                            $('#form-group-pass'+item_id).hide().delay("fast");
                            $('#titleModal'+item_id).prop('disabled', false);
                            $('#bodyModal'+item_id).prop('disabled', false);
                            $('#buttonModal'+item_id).prop('disabled', false);
                            $('#checkModal'+item_id).prop('disabled', false);

                            $('#erroMsgPass'+item_id).hide();
                            $('#btnEditModal'+item_id).hide();
                            $('#btnSaveModal'+item_id).show();

                        } else {

                            $('#titleModal'+item_id).prop('disabled', true);
                            $('#bodyModal'+item_id).prop('disabled', true);
                            $('#buttonModal'+item_id).prop('disabled', true);
                            $('#checkModal'+item_id).prop('disabled', true);

                            $('#erroMsgPass'+item_id).show();
                        }
                        
                    }else{

                        $('#erroMsgPass'+item_id).show();
                    }
                    /*console.log($.isNumeric(valuePasswordModal));
                    /*if(valuePasswordModal == passTrue) {
                        $('#form-group-pass'+item_id).hide().delay("fast");
                        $('#titleModal'+item_id).prop('disabled', false);
                        $('#bodyModal'+item_id).prop('disabled', false);
                        $('#buttonModal'+item_id).prop('disabled', false);
                        $('#checkModal'+item_id).prop('disabled', false);

                        console.log("jawaban benar");
                    } else {
                        $('#titleModal'+item_id).prop('disabled', true);
                        $('#bodyModal'+item_id).prop('disabled', true);
                        $('#buttonModal'+item_id).prop('disabled', true);
                        $('#checkModal'+item_id).prop('disabled', true);
                        
                        console.log("jawaban salah");
                    } */
                }
            })
        }
        /*
        function onClickSaveModal(item_id)
        {
            event.preventDefault();
            console.log(item_id);/*
            let title = $("input[name=title]"+item_id).val();
            let body = $("input[name=body]").val();
            let image = $("input[name=image]").val();
            let _token   = $('meta[name="csrf-token"]').attr('content');

            let title = $('#titleModal'+item_id).val();
            let body = $('#bodyModal'+item_id).val();
            let image = $('#buttonModal'+item_id).val().replace(/C:\\fakepath\\/i, '');
            let _token   = $('meta[name="csrf-token"]').attr('content');
            
            console.log(image);

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                 }
            });

            $.ajax({
                url: "http://127.0.0.1:8000/api/edit/bulletin/"+item_id,
                type: "POST",
                data:{
                    title:title,
                    body:body,
                    images:image,
                    contentType:false,
                    processData:false,
                    _token:_token
                },
                success:function(response)
                {
                    console.log(response);
                    console.log("sukses");
                },
            });
        }*/

        // INPUT TYPE FILE
        $(document).on('change', '.btn-file :file', function() {
          var input = $(this),
              numFiles = input.get(0).files ? input.get(0).files.length : 1,
              label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
          input.trigger('fileselect', [numFiles, label]);
        });
  
        $(document).ready( function() {
          $('.btn-file :file').on('fileselect', function(event, numFiles, label) {
            var input = $(this).parents('.input-group').find(':text'),
                log = numFiles > 1 ? numFiles + ' files selected' : label;
  
            if( input.length ) {
              input.val(log);
            } else {
              if( log ) alert(log);
            }
          });
        });
      </script> 
@endsection