<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Bulletin extends Model
{
    protected $fillable = ['title','body','images','password'];
}
