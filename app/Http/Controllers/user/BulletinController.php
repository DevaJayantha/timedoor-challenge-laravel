<?php

namespace App\Http\Controllers\user;

use App\Http\Controllers\Controller;
use App\Http\Requests\BulletinFormRequest;
use Illuminate\Http\Request;
use App\Model\Bulletin;
use File;

use Illuminate\Support\Facades\Crypt;
use Intervention\Image\Exception\NotReadableException;

class BulletinController extends Controller
{
    public function index()
    {
        $user = Bulletin::latest()->paginate(10);
        return view('user.index',['userItem'=> $user]);
        
    }

    public function store(BulletinFormRequest $request)
    {
        // get all input in form
        $input = $request->all();

        // hashing password
        // $hash_password = Hash::make($request->get('password'));
        // $input['password'] = $hash_password;
        $encrypted = Crypt::encryptString($request->get('password'));
        $input['password'] = $encrypted;
        // cek file image 
        if($request->hasFile('image'))
        {
            // change name file & save in folder
            $file = $request->file('image');
            $fill_extension = $file->getClientOriginalName();
            $destination_path = 'user\images';
            $fillname = 'bulletin_' . date('YmdHis') . '_' .$fill_extension;
            $request->file('image')->move($destination_path,$fillname);
            $input['images'] = $fillname;

            // $extension = $file->getClientOriginalExtension();
            // $fillpath = 'bulletin_' . date('YmdHis') . '.' .$extension;
            // $file->move('user\images',$fillpath);
        }

        $post = Bulletin::create($input);
        // $post = Bulletin::create([
        //     'title' =>  $request->title,
        //     'body'  => $request->body,
        //     'images' => $request->image=$file,
        //     'password' => $request->password
        // ]);

        return redirect('/')->with(['success' => 'Pesan Berhasil']);
    }

    public function getIdBulletin($id)
    {
        $get = Bulletin::find($id);
        
        $decrypted = Crypt::decryptString($get['password']);
        
        $message=[
            'success' => true,
            'data'   => $decrypted,
        ];
        
        return response($message);
    }

    public function showAllData()
    {
        $get = Bulletin::all();

        $message=[
            'success' => true,
            'data'    => $get
        ];

        return response($message);
    }
    public function editBulletin(Request $request, $id)
    {
        $edit = Bulletin::find($id);
        $edit->title = $request['title'];
        $edit->body = $request['body'];
        
        // print($request['images']);
        // if(null !== $request->get('images'))
        // {
        //     if($request->get('image'))
        //     {
        //         // $image_str = $request->get('image');
        //         // $array = explode(',', $image_str);
        //         // $extension = explode('/', explode(':', substr($image_str, 0, strpos($image_str, ';')))[1])[1];
        //         // $filePic = Image::make($array[1])->encode($extension); 

        //         // $fillname = 'bulletin_images_' . date('YmdHis');
        //         // $path='user/images/';
        //         // $filepic = ($path . $fillname. '.' .$extension);

        //         // $edit->images = $path . $fillname. '.' .$extension;

 
        //     }
        // }
        if($request->hasFile('image'))
        {

            // change name file & save in folder
            $file = $request->file('image');
            $fill_extension = $file->getClientOriginalName();
            $destination_path = 'user\images';
            $fillname = 'bulletin_' . date('YmdHis') . '_' .$fill_extension;
            $edit->images = $request->file('image')->move($destination_path,$fillname);
            $edit['images'] = $fillname;

            // $extension = $file->getClientOriginalExtension();
            // $fillpath = 'bulletin_' . date('YmdHis') . '.' .$extension;
            // $file->move('user\images',$fillpath);
        }
         else {
            $edit['images'] = 'image-null.png';
        }
        // $edit->images = $request['images'];
        
        $edit->save();

        $message = [
            'message' => 'update sukses',
            'data'    => $edit,
        ];

        return response($message);
    }

    public function editBulletinModal(Request $request, $id)
    {

        $edit = Bulletin::find($id);
        $edit->title = $request['title'];
        $edit->body  = $request['body'];
        

        if($request['checkbox'] == true)
        {
            // File::delete('user/images/'.$edit->images);
            unlink(public_path().'/user/images/' .$edit->images);

        }
        else {
            if($request->hasFile('image'))
            {
                // change name file & save in folder
                $file = $request->file('image');
                $fill_extension = $file->getClientOriginalName();
                $destination_path = 'user\images';
                $fillname = 'bulletin_' . date('YmdHis') . '_' .$fill_extension;
                $request->file('image')->move($destination_path,$fillname);
                $edit['images'] = $fillname;
    
                // $extension = $file->getClientOriginalExtension();
                // $fillpath = 'bulletin_' . date('YmdHis') . '.' .$extension;
                // $file->move('user\images',$fillpath);
            }
        }
        
        $edit->save();


        return redirect('/')->with(['success' => 'Pesan Berhasil']);
    }


}
