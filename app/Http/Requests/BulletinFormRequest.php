<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BulletinFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'     => 'required|min:10|max:32',
            'body'      => 'required|min:10|max:200',
            'image'     => 'required|image|mimes:jpeg,jpg,png,gif|max:1000',
            'password'  => 'required|numeric|digits:4',
        ];
    
    }
    public function messages()
    {
        return [
            'title.required'      => 'Value title must be filled in',
            'body.required'       => 'Value body must be filled in',
            'password.required'   => 'Value password must be filled in',
            'image.required'      => 'Value images must be filled in',
            'title.min'           => 'Your title must be 10 to 32 characters long',
            'title.max'           => 'Your title must be 10 to 32 characters long',
            'body.min'            => 'Your body must be 10 to 200 characters long',
            'body.max'            => 'Your body must be 10 to 200 characters long',
            'password.numeric'    => 'Your password must be 4 digit number',
            'password.digits'     => 'Your password must be 4 digit number',
            'image.image'         => 'Your input only image format',
            'image.max'           => 'Your image is only valid 1MB or less',
            'image.mimes'         => 'Your image is only valid .jpeg, .jpg, .png, .gif',
        ];
    }
}
